FROM node:10 as build


WORKDIR /app
ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
COPY package*.json /app/
RUN npm install
COPY . /app

ENV API_URL=https://marketplace-bresciastone-backend.bangun-kreatif.com/v1/
ENV IMAGE_URL=https://marketplace-bresciastone-backend.bangun-kreatif.com
ENV APP_LOGO=https://marketplace-bresciastone-backend.bangun-kreatif.com/assets/images/logo.png

RUN npm run build

FROM nginx:stable-alpine
COPY --from=build /app/dist /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
